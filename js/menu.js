var nav = document.getElementById("nav");

if (localStorage.length === 0) {
    var pics = [{
            src: "img/nature1.jpg",
            title: "Nature"
        }, {
            src: "img/people2.jpg",
            title: "People"
        },
        {
            src: "img/arch1.jpg",
            title: "Architecture"
        }, {
            src: "img/nature2.jpg",
            title: "Nature"
        },
        {
            src: "img/arch2.jpg",
            title: "Architecture"
        }, {
            src: "img/people1.jpg",
            title: "People"
        }, {
            src: "img/nature3.jpg",
            title: "Nature"
        },
        {
            src: "img/arch3.jpg",
            title: "Architecture"
        }, {
            src: "img/people3.jpg",
            title: "People"
        },
        {
            src: "img/nature4.jpg",
            title: "Nature"
        }, {
            src: "img/people4.jpg",
            title: "People"
        }, {
            src: "img/arch4.jpg",
            title: "Architecture"
        }, {
            src: "img/nature5.jpg",
            title: "Nature"
        }, {
            src: "img/nature6.jpg",
            title: "Nature"
        },
        {
            src: "img/nature7.jpg",
            title: "Nature"
        }, {
            src: "img/nature8.jpg",
            title: "Nature"
        }, {
            src: "img/interior1.jpg",
            title: "Interiors"
        }, {
            src: "img/people5.jpg",
            title: "People"
        }, {
            src: "img/people6.jpg",
            title: "People"
        },
        {
            src: "img/interior2.jpg",
            title: "Interiors"
        }, {
            src: "img/people7.jpg",
            title: "People"
        }, {
            src: "img/people8.jpg",
            title: "People"
        }, {
            src: "img/people9.jpg",
            title: "People"
        }, {
            src: "img/interior3.jpg",
            title: "Interiors"
        },
        {
            src: "img/interior4.jpg",
            title: "Interiors"
        }, {
            src: "img/interior5.jpg",
            title: "Interiors"
        }, {
            src: "img/interior6.jpg",
            title: "Interiors"
        }
    ];
    var interiors = ["img/interior1.jpg", "img/interior2.jpg", "img/interior3.jpg",
        "img/interior4.jpg", "img/interior5.jpg", "img/interior6.jpg"
    ];
    var people = ["img/people1.jpg", "img/people2.jpg", "img/people3.jpg", "img/people4.jpg",
        "img/people5.jpg", "img/people6.jpg", "img/people7.jpg", "img/people8.jpg", "img/people9.jpg"
    ];
    var nature = ["img/nature1.jpg", "img/nature2.jpg", "img/nature3.jpg", "img/nature4.jpg",
        "img/nature5.jpg", "img/nature6.jpg", "img/nature7.jpg", "img/nature8.jpg"
    ];
    localStorage.setItem('pics', JSON.stringify(pics));
    localStorage.setItem('interiors', JSON.stringify(interiors));
    localStorage.setItem('people', JSON.stringify(people));
    localStorage.setItem('nature', JSON.stringify(nature));
}

function showSelect() {
    nav.classList.toggle('show');
    document.getElementById('card-add') ?
        document.getElementById('card-add').classList.toggle('show-off') :
        document.getElementById('card-add');
}

function picturesGrid(type) {
    var showPics = ''
    var picsStorage = JSON.parse(localStorage.getItem('pics'));
    var interiorsStorage = JSON.parse(localStorage.getItem('interiors'));
    var natureStorage = JSON.parse(localStorage.getItem('nature'));
    var peopleStorage = JSON.parse(localStorage.getItem('people'));

    if (type === "all") {
        for (let i in picsStorage) {
            var htmlBuilderResult = htmlBuilder(picsStorage[i].src, i, picsStorage[i].title, "'all'");
            showPics = showPics.concat(htmlBuilderResult);
        };
    } else if (type === "interior") {
        for (let i in interiorsStorage) {
            var htmlBuilderResult = htmlBuilder(interiorsStorage[i], i, 'Interiors', "'interiors'");
            showPics = showPics.concat(htmlBuilderResult);
        };
    } else if (type === "people") {
        for (let i in peopleStorage) {
            var htmlBuilderResult = htmlBuilder(peopleStorage[i], i, 'People', "'people'");
            showPics = showPics.concat(htmlBuilderResult);
        };
    } else if (type === "nature") {
        for (let i in natureStorage) {
            var htmlBuilderResult = htmlBuilder(natureStorage[i], i, 'Nature', "'nature'");
            showPics = showPics.concat(htmlBuilderResult);
        };
    };
    var response = document.write(showPics);
    return response;
}

function htmlBuilder(src, index, title, type) {
    var html = '<div class="grid" id="' + src +
        '"><span class="icon-close" id="btn-menu" onclick="deleteImg(' + index + ',' + type + ')' +
        '"></span><a class="img-link" href=' + src +
        ' target="_blank"><img src=' + src +
        ' class="img__animate"></a><div class="grid__body"><div class="relative"><a class="grid__link" target="_blank" href="/"></a><h1 class="grid__title fade-in">' + title + '</h1></div></div></div>';
    return html;
}

function deleteImg(index, type) {
    if (type === "nature") {
        var natureStorage = JSON.parse(localStorage.getItem('nature'));
        document.getElementById(natureStorage[parseInt(index)]).animate([{
                transform: 'translateY(0px)'
            },
            {
                transform: 'translateY(-300px)'
            }
        ], {
            duration: 1500,
            iterations: Infinity
        });
        natureStorage.splice(parseInt(index), 1);
        localStorage.setItem('nature', JSON.stringify(natureStorage));
        location.href = "nature.html";

    }

    if (type === "people") {
        var peopleStorage = JSON.parse(localStorage.getItem('people'));
        document.getElementById(peopleStorage[parseInt(index)]).animate([{
                transform: 'translateY(0px)'
            },
            {
                transform: 'translateY(-300px)'
            }
        ], {
            duration: 1500,
            iterations: Infinity
        });
        peopleStorage.splice(parseInt(index), 1);
        localStorage.setItem('people', JSON.stringify(peopleStorage));
        location.href = "people.html";
    }

    if (type === "interiors") {
        var interiorsStorage = JSON.parse(localStorage.getItem('interiors'));
        document.getElementById(interiorsStorage[parseInt(index)]).animate([{
                transform: 'translateY(0px)'
            },
            {
                transform: 'translateY(-300px)'
            }
        ], {
            duration: 1500,
            iterations: Infinity
        });
        interiorsStorage.splice(parseInt(index), 1);
        localStorage.setItem('interiors', JSON.stringify(interiorsStorage));
        location.href = "interior.html";
    }

    if (type === "all") {
        var picsStorage = JSON.parse(localStorage.getItem('pics'));
        document.getElementById(picsStorage[parseInt(index)].src).animate([{
                transform: 'translateY(0px)'
            },
            {
                transform: 'translateY(-300px)'
            }
        ], {
            duration: 1500,
            iterations: Infinity
        });
        picsStorage.splice(parseInt(index), 1);
        localStorage.setItem('pics', JSON.stringify(picsStorage));
        location.href = "test.html";
    }
}

function addImage() {
    newImgUrl = document.getElementById("urlimg").value;
    newDirUrl = document.getElementById("urldir").value;
    newType = document.getElementById("urltype").value;

    var picsStorage = JSON.parse(localStorage.getItem('pics'));
    var interiorsStorage = JSON.parse(localStorage.getItem('interiors'));
    var natureStorage = JSON.parse(localStorage.getItem('nature'));
    var peopleStorage = JSON.parse(localStorage.getItem('people'));

    if (newType === "Nature") {
        natureStorage.push(newImgUrl)
        localStorage.setItem('nature', JSON.stringify(natureStorage))
    };

    if (newType === "People") {
        peopleStorage.push(newImgUrl)
        localStorage.setItem('people', JSON.stringify(peopleStorage))
    };

    if (newType === "Interiors") {
        interiorsStorage.push(newImgUrl)
        localStorage.setItem('interiors', JSON.stringify(interiorsStorage))
    };

    picsStorage.push({
        src: newImgUrl,
        title: newType
    });

    localStorage.setItem('pics', JSON.stringify(picsStorage));
}

function restore() {
    localStorage.clear();
}